<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin,Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    include_once "../../config/redbean.php";
    include_once '../../model/user.php';
    
    $user = new User();

    $data = json_decode(file_get_contents("php://input"));

    $user->id = htmlspecialchars(strip_tags($data->id));

    $result = R::findOne($user->getTableName(), 'id = ?', 
    [$user->id]);

    $respond = array();
    $respond["message"] = "";
    $respond["data"] = "";
    $respond["status"] = false;

    if($result != null){
        $respond["data"] = $result->export();
        $respond["message"] = "Found";
        $respond["status"] = true;
        echo json_encode($respond);

    }
    else{
        $respond["message"] = "Not Found";
        $respond["status"] = false;
        echo json_encode($respond);
    }

    R::close();

?>