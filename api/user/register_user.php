<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
    session_start();

    include_once "../../config/redbean.php";
    include_once '../../model/user.php';
    include_once '../../model/user_type.php';
    $user = new User();

    $data = json_decode(file_get_contents("php://input"));

    $user->username = htmlspecialchars(strip_tags($data->username));
    $user->password = htmlspecialchars(strip_tags($data->password));
    $user->user_type_id = htmlspecialchars(strip_tags($data->user_type_id));

    $existed = R::find($user->getTableName(), 'BINARY username = BINARY ?', [$user->username]);

    $respond = array();
    $respond["message"] = "";
    $respond["status"] = false;

    if ($existed == null){
        $new_user = R::dispense($user->getTableName());
        $new_user->username = $user->username;
        $new_user->password = password_hash($user->password, PASSWORD_BCRYPT);
        $new_user->user_type_id = $user->user_type_id;
        $result = R::store($new_user);
        
        if($result != null){
            $respond["message"] = "Registration success";
            $respond["status"] = true;
            echo json_encode($respond);
        }
        else{
            $respond["message"] = "Registration failed";
            $respond["status"] = false;
            echo json_encode($respond);
        }
    }
    else{
        $respond["message"] = "Username already existed";
        $respond["status"] = false;
        echo json_encode($respond);
    }

    R::close();

?>