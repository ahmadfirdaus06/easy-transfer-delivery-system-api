<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin,Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
    // session_start();

    include_once "../../config/redbean.php";
    include_once '../../model/user.php';
    $user = new User();

    $data = json_decode(file_get_contents("php://input"));

    $user->username = htmlspecialchars(strip_tags($data->username));
    $user->password = htmlspecialchars(strip_tags($data->password));
    $user->user_type_id = htmlspecialchars(strip_tags($data->user_type_id));

    $result = R::findOne($user->getTableName(), '
    BINARY username = BINARY ? &&
    user_type_id = ?'
    , [$user->username, $user->user_type_id]);

    $respond = array();
    $respond["message"] = "";
    $respond["data"] = "";
    $respond["status"] = false;

    date_default_timezone_set("Asia/Kuala_Lumpur");

    if($result != null){
        if (password_verify($user->password, $result->password)){
            
            // $_SESSION['user'] = $result->export();
            $respond["data"] = $result->export();
            $result->last_login = date("Y-m-d H:i:s");
            $result->updated_at = date("Y-m-d H:i:s");
            
            // if (isset($_SESSION['user']) && !empty($_SESSION['user'] && R::store($result) != null)){    
            //     $respond["message"] = "Login success";
            //     $respond["status"] = true;
            //     echo json_encode($respond);    
            // }
            if (R::store($result) != null){    
                $respond["message"] = "Login Success";
                $respond["status"] = true;
                echo json_encode($respond);    
            }
            else{
                $respond["message"] = "Login Failed";
                $respond["status"] = false;
                echo json_encode($respond);
            }
        } 
        else{
            $respond["message"] = "Password Incorrect";
            $respond["status"] = false;
            echo json_encode($respond);
        }
    }
    else{
        $respond["message"] = "Account not existed";
        $respond["status"] = false;
        echo json_encode($respond);
    }

    R::close();

?>