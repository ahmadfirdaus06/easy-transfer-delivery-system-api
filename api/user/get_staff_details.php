<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST, GET');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin,Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    include_once "../../config/redbean.php";
    include_once '../../model/user.php';
    $user = new User();

    $result = null;

    $respond = array();
    $respond["message"] = "";
    $respond["data"] = "";
    $respond["status"] = false;

    switch($_SERVER['REQUEST_METHOD']){
        case "POST":
            $data = json_decode(file_get_contents("php://input"));
            $user->id = htmlspecialchars(strip_tags($data->id));
            $result = R::findOne($user->getTableName(), '
            user_type_id = ? &&
            id = ?'
            , ["3", $user->id]);
            if ($result != null){
                $respond["message"] = "Found";
                $respond["status"] = true;
                $respond["data"] = $result->export();
            }
            break;

        case "GET":
            $result = R::find($user->getTableName(), '
            user_type_id = ? ORDER BY updated_at DESC'
            , ["3"]);
            $respond["message"] = "Found";
            $respond["status"] = true;
            $respond["data"] = [];
            foreach($result as $item){
                $item = json_decode(json_encode($item));
                $arr = array(
                    'id' => $item->id,
                    'nric' => $item->nric,
                    'username' => $item->username,
                    'password' => $item->password,
                    'name' => $item->name,
                    'email' => $item->email,
                    'created_at' => $item->created_at,
                    'updated_at' => $item->updated_at,
                    'last_login' => $item->last_login,
                    'last_logout' => $item->last_logout,
                    'phone_num' => $item->phone_num,
                    'user_type_id' => $item->user_type_id,
                    'user_status_id' => $item->user_status_id
                );
                array_push($respond["data"], $arr);
            }
            break;
    }

    echo json_encode($respond);

    R::close();

?>