<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    session_start();

    $respond = array();
    $respond["data"] = "";
    $respond["status"] = false;

    if (isset($_SESSION["user"]) && !empty($_SESSION["user"])){
        $respond["status"] = true;
        $respond["data"] = $_SESSION["user"];
        echo json_encode($respond);
    }
    else{
        $respond["status"] = false;
        echo json_encode($respond); 
    }
?>