<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
    session_start();

    include_once "../../config/redbean.php";
    include_once '../../model/user.php';
    $user = new User();

    $respond = array();
    $respond["message"] = "";
    $respond["status"] = false;

    date_default_timezone_set("Asia/Kuala_Lumpur");

    if (isset($_SESSION["user"]) && !empty($_SESSION["user"])){
        $user = R::load($user->getTableName(), $_SESSION["user"]["id"]);
        if (session_destroy()){
            $user->last_logout = date("Y-m-d H:i:s");
            $user->updated_at = date("Y-m-d H:i:s");
            $update_log = R::store($user);
            if ($update_log != null){
                $respond["message"] = "Logout success";
                $respond["status"] = true; 
                echo json_encode($respond);    
            }
            else{
                $respond["message"] = "Log not updated";
                $respond["status"] = true; 
                echo json_encode($respond);
            }
        }
        else{
            $respond["message"] = "Logout failed";
            $respond["status"] = false; 
            echo json_encode($respond);
        }
    }
    else{
        $respond["message"] = "Session not found";
        $respond["status"] = true;
        echo json_encode($respond);
    }

    R::close();
?>