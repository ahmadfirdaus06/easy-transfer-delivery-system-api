<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin,Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    include_once "../../config/redbean.php";
    include_once '../../model/company.php';
    $company = new Company();

    $data = json_decode(file_get_contents("php://input"));

    $company->id = htmlspecialchars(strip_tags($data->id));
    $company->company_name = htmlspecialchars(strip_tags($data->company_name));
    $company->desc = htmlspecialchars(strip_tags($data->desc));
    $company->about = htmlspecialchars(strip_tags($data->about));
    $company->establish_at = htmlspecialchars(strip_tags($data->establish_at));

    date_default_timezone_set("Asia/Kuala_Lumpur");

    $respond = array();
    $respond["message"] = "";
    $respond["status"] = false;

    // $fetched_company = R::load($company->getTableName(), $company->id);
    $fetched_company = R::findOne($company->getTableName(), '
    id = ?'
    , [$company->id]);
    
    $fetched_company->company_name = $company->company_name;
    $fetched_company->desc = $company->desc;
    $fetched_company->about = $company->about;
    $fetched_company->establish_at = $company->establish_at;
    $fetched_company->updated_at = date("Y-m-d H:i:s");

    $result = R::store($fetched_company);

    if($result != null){
        $respond["message"] = "Changes Saved";
        $respond["status"] = true;
    }
    else{
        $respond["message"] = "Cannot Save Changes";
        $respond["status"] = false;
    }
    echo json_encode($respond);

    R::close();

?>