<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin,Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    include_once "../../config/redbean.php";
    include_once '../../model/company.php';
    
    $company = new Company();

    $result = R::findOne($company->getTableName());

    $respond = array();
    $respond["message"] = "";
    $respond["data"] = "";
    $respond["status"] = false;

    if($result != null){
        $respond["data"] = $result->export();
        $respond["message"] = "Found";
        $respond["status"] = true;
        echo json_encode($respond);

    }
    else{
        $respond["message"] = "Not Found";
        $respond["status"] = false;
        echo json_encode($respond);
    }

    R::close();

?>