<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST, GET');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin,Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    include_once "../../config/redbean.php";
    include_once "../../model/report.php";

    $report = new Report();

    $data = json_decode(file_get_contents("php://input"));

    $respond = array();
    $respond["data"] = [];
    $respond["status"] = false;
    $result = [];


    if ($_SERVER["REQUEST_METHOD"] == "POST"){
        $report->driver_id = htmlspecialchars(strip_tags($data->driver_id));
        $result = R::find($report->getTableName(),
        " driver_id = ? ORDER BY updated_at DESC",
        [$report->driver_id]);
        $respond["status"] = true;
    }
    
    if ($_SERVER["REQUEST_METHOD"] == "GET"){
        $result = R::findAll($report->getTableName(), " ORDER BY updated_at DESC");
        $respond["status"] = true;
    } 

    foreach($result as $item){
        $item = json_decode(json_encode($item));
        $arr = array(
            'id' => $item->id,
            'customer_order_id' => $item->customer_order_id,
            'driver_id' => $item->driver_id,
            'total_rating' => $item->total_rating,
            'total_earned' => $item->total_earned,
            'created_at' => $item->created_at,
            'updated_at' => $item->updated_at
        );
        array_push($respond["data"], $arr);
    }

    echo json_encode($respond);
?>