<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin,Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    include_once "../../config/redbean.php";
    include_once "../../model/user.php";
    include_once "../../model/staff_join_request.php";

    $user = new User();
    $join_request = new StaffJoinRequest();

    $data = json_decode(file_get_contents("php://input"));

    $user->user_type_id = htmlspecialchars(strip_tags($data->user_type_id));

    $check_main = [];
    $check_sub = [];

    if (property_exists($data, "email")){
        $user->email = htmlspecialchars(strip_tags($data->email));
        $check_main = R::find($user->getTableName(), 'BINARY email = BINARY ?', [$user->email]);
        $check_sub = R::find($join_request->getTableName(), 'BINARY email = BINARY ?', [$user->email]);

    }
    else if (property_exists($data, "username")){
        $user->username = htmlspecialchars(strip_tags($data->username));
        $check_main = R::find($user->getTableName(), 'BINARY username = BINARY ?', [$user->username]);
        $check_sub = R::find($join_request->getTableName(), 'BINARY username = BINARY ?', [$user->username]);
    }

    $respond = array();
    $respond["message"] = "";
    $respond["status"] = false;

    if (empty($check_main) && empty($check_sub)){
        $respond["message"] = "Not exist";
        $respond["status"] = true;
        echo json_encode($respond);
    }
    else{
        $respond["message"] = "Exist";
        $respond["status"] = false;
        echo json_encode($respond); 
    }
    
    R::close();

?>