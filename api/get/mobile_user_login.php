<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin,Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    include_once "../../config/redbean.php";
    include_once "../../model/user.php";
    
    $user = new User();

    $data = json_decode(file_get_contents("php://input"));
    $user->username = htmlspecialchars(strip_tags($data->username));
    $user->password = htmlspecialchars(strip_tags($data->password));

    $respond = array();
    $respond["message"] = "";
    $respond["data"] = [];
    $respond["error_flag"] = true;
    $respond["status"] = false;

    $result = R::findOne($user->getTableName(), 'BINARY username = BINARY ?', [$user->username]);

    if (!empty($result)){
        if (password_verify($user->password, $result->password)){
            if ($result->user_type_id != 1){
                $respond["message"] = "Login Successful";
                $respond["data"] = $result;
                $respond["status"] = true;
                $respond["error_flag"] = false;
            }
            else{
                $respond["message"] = "Cannot login using this account";
                $respond["status"] = false; 
                $respond["error_flag"] = false;
            }
        }
        else{
            $respond["message"] = "Wrong Password";
            $respond["error_flag"] = true;
            $respond["status"] = false;
        }
    }
    else{
        $respond["message"] = "Wrong Username";
        $respond["error_flag"] = true;
        $respond["status"] = false;
    }

    echo json_encode($respond);

    R::close();

?>