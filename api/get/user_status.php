<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin,Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    include_once "../../config/redbean.php";
    include_once '../../model/user_status.php';
    $user_status = new UserStatus();

    $result = null;

    $respond = array();
    $respond["message"] = "";
    $respond["data"] = "";
    $respond["status"] = false;

    $data = json_decode(file_get_contents("php://input"));
    $user_status->id = htmlspecialchars(strip_tags($data->id));
    $result = R::findOne($user_status->getTableName(), '
    id = ?'
    , [$user_status->id]);
    $respond["message"] = "Found";
    $respond["status"] = true;
    $respond["data"] = $result->export();

    echo json_encode($respond);

    R::close();

?>