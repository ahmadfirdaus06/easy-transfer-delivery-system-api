<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin,Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    include_once "../../config/redbean.php";
    include_once "../../model/parcel_size.php";
    
    $parcel = new ParcelSize();

    $data = json_decode(file_get_contents("php://input"));
    $parcel->id = htmlspecialchars(strip_tags($data->id));

    $respond = array();
    $respond["message"] = "";
    $respond["data"] = [];
    $respond["status"] = false;

    $result = R::findOne($parcel->getTableName(), ' id = ?', [$parcel->id]);

    if (!empty($result)){
        $respond["data"] = $result;
        $respond["status"] = true;
    }
    else{
        $respond["status"] = false;
    }

    echo json_encode($respond);

    R::close();

?>