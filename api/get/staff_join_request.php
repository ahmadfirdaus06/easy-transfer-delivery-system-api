<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST, GET');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin,Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    include_once "../../config/redbean.php";
    include_once '../../model/staff_join_request.php';
    $staff = new StaffJoinRequest();

    $result = [];

    $respond = array();
    $respond["message"] = "";
    $respond["data"] = [];
    $respond["status"] = false;

    switch($_SERVER['REQUEST_METHOD']){
        case "POST":
            $data = json_decode(file_get_contents("php://input"));
            $staff->id = htmlspecialchars(strip_tags($data->id));
            $result = R::find($staff->getTableName(), 'id = ?', [$staff->id]);
            break;

        case "GET":
            $result = R::findAll($staff->getTableName(), 'ORDER BY created_at ASC');
            break;
    }

    if (!empty($result)){
        $respond["message"] = "Found";
        $respond["status"] = true;

        foreach($result as $item){
            array_push($respond["data"], $item);
        }
    }
    else{
        $respond["message"] = "Not Found";
        $respond["status"] = false;
    }

    echo json_encode($respond);

    R::close();

?>