<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST, GET');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin,Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    include_once "../../config/db.php";
    include_once '../../model/customer_order.php';
    include_once '../../model/user.php';
    include_once '../../model/payment.php';
    include_once '../../model/card.php';

    $db = new Database();
    $order = new Order();
    $user = new User();

    $respond = array();
    $respond["data"] = [];
    $respond["status"] = false;

    $conn = $db->connect();

    $stmt = null;

    $sql = "
    SELECT 
    dt_customer_order.`id`,
    dt_customer_order.`updated_at`,
    dt_customer_order.`pickup_date_time`, 
    dt_customer_order.`dropoff_date_time`, 
    dt_customer_order.`pickup_location`,
    dt_customer_order.`delivery_distance`,
    dt_customer_order.`dropoff_location`, 
    dt_customer_order.`order_accept_by`, 
    dt_customer_order.`customer_id`,
    customer.`name` AS customer_name,
    customer.`phone_num` AS customer_phone_num,
    dt_customer_order.`driver_id`, 
    driver.`name` AS driver_name,
    driver.`phone_num` AS driver_phone_num,
    dt_customer_order.`parcel_size_id`, 
    lt_parcel_size_rate.`desc` AS parcel_size_desc,
    dt_customer_order.`order_status_id`, 
    lt_order_status.`desc` AS order_status_desc,
    dt_customer_order.`created_at`,
    dt_payment.`total_cost_delivery` AS total_cost_delivery,
    dt_payment.`payment_status_id` AS payment_status_id,
    (SELECT lt_payment_status.`desc` FROM lt_payment_status WHERE lt_payment_status.`id` = dt_payment.`payment_status_id`) AS payment_status_desc,
    dt_payment.`payment_method_id` AS payment_method_id,
    (SELECT lt_payment_method.`desc` FROM lt_payment_method WHERE lt_payment_method.`id` = dt_payment.`payment_method_id`) AS payment_method_desc
    FROM
    dt_customer_order
    LEFT JOIN dt_user AS customer ON
    dt_customer_order.`customer_id` = customer.`id`
    LEFT JOIN dt_user AS driver on
    dt_customer_order.`driver_id` = driver.`id`
    LEFT JOIN lt_parcel_size_rate ON
    dt_customer_order.`parcel_size_id` = lt_parcel_size_rate.`id`
    LEFT JOIN lt_order_status ON
    dt_customer_order.`order_status_id` = lt_order_status.`id`
    LEFT JOIN dt_payment ON
    dt_customer_order.`id` = dt_payment.`customer_order_id`
    ";

    if ($_SERVER["REQUEST_METHOD"] == "POST"){
        
        $data = json_decode(file_get_contents("php://input"));

        if (property_exists($data, "user_type_id")){
            $user->user_type_id = htmlspecialchars(strip_tags($data->user_type_id));
            
            if ($user->user_type_id == 2){

                $user->id = htmlspecialchars(strip_tags($data->customer_id));

                if (property_exists($data, "recent")){
                    $sql = $sql . " WHERE customer_id = :customer_id && order_status_id < 4";
                }
                else{
                    $sql = $sql . " WHERE customer_id = :customer_id";
                }
                
                $stmt = $conn->prepare($sql . " ORDER BY created_at DESC");
                $stmt->bindParam(':customer_id', $user->id);
            }
            else if ($user->user_type_id == 3){

                if (property_exists($data, "recent")){
                    $user->id = htmlspecialchars(strip_tags($data->driver_id));
                    $sql = $sql . " WHERE order_status_id < 3 && driver_id = :driver_id";
                    $stmt = $conn->prepare($sql . " ORDER BY created_at DESC");
                    $stmt->bindParam(':driver_id', $user->id);
                }
                else{
                    $sql = $sql . " WHERE order_status_id < 3";
                    $stmt = $conn->prepare($sql . " ORDER BY created_at DESC");
                }
                
            }
        }
        else if (property_exists($data, "order_id")){
            $order->id = htmlspecialchars(strip_tags($data->order_id));
            $sql = $sql . " WHERE dt_customer_order.id = :id";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':id', $order->id);
        }
    }
    else if ($_SERVER["REQUEST_METHOD"] == "GET"){
        $stmt = $conn->prepare($sql . " ORDER BY created_at DESC");
    }

    if ($stmt->execute()){

        $respond["status"] = true;

        if ($stmt->rowCount() > 0){
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                extract($row);
                $data = array(
                    'id' => $id,
                    'updated_at' => $updated_at,
                    'created_at' => $created_at,
                    'pickup_date_time' => $pickup_date_time,
                    'dropoff_date_time' => $dropoff_date_time,
                    'pickup_location' => $pickup_location,
                    'dropoff_location' => $dropoff_location,
                    'order_accept_by' => $order_accept_by,
                    'customer_id' => $customer_id,
                    'customer_name' => $customer_name,
                    'customer_phone_num' => $customer_phone_num,
                    'driver_id' => $driver_id,
                    'driver_name' => $driver_name,
                    'driver_phone_num' => $driver_phone_num,
                    'parcel_size_id' => $parcel_size_id,
                    'parcel_size_desc' => $parcel_size_desc,
                    'order_status_id' => $order_status_id,
                    'order_status_desc' => $order_status_desc,
                    'total_cost_delivery' => $total_cost_delivery,
                    'payment_status_id' => $payment_status_id,
                    'payment_status_desc' => $payment_status_desc,
                    'payment_method_id' => $payment_method_id,
                    'payment_method_desc' => $payment_method_desc,
                    'delivery_distance' => $delivery_distance
                );
    
                array_push($respond['data'], $data);
            }
        }
    }
    else{
        $respond["status"] = false;
    }

    echo json_encode($respond);

    $conn = null;
?>