<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    include_once "../../config/redbean.php";
    include_once '../../model/user.php';
    include_once '../../model/staff_join_request.php';
    $user = new User();
    $join_request = new StaffJoinRequest(); 

    $data = json_decode(file_get_contents("php://input"));

    $join_request->nric = htmlspecialchars(strip_tags($data->nric));
    $join_request->username = htmlspecialchars(strip_tags($data->username));
    $join_request->password = htmlspecialchars(strip_tags($data->password));
    $join_request->name = htmlspecialchars(strip_tags($data->name));
    $join_request->email = htmlspecialchars(strip_tags($data->email));
    $join_request->phone_num = htmlspecialchars(strip_tags($data->phone_num));

    // $existed_1 = R::find($join_request->getTableName(), 'BINARY username = BINARY ?', [$join_request->username]);
    // $existed_2 = R::find($user->getTableName(), 'BINARY username = BINARY ?', [$join_request->username]);
    
    $respond = array();
    $respond["message"] = "";
    $respond["status"] = false;

    // if (empty($existed_1) && empty($existed_2)){
        $new_request = R::dispense($join_request->getTableName());
        $new_request->nric = $join_request->nric;
        $new_request->username = $join_request->username;
        $new_request->password = password_hash($join_request->password, PASSWORD_BCRYPT);
        $new_request->name = $join_request->name;
        $new_request->email = $join_request->email;
        $new_request->phone_num = $join_request->phone_num;
        $result = R::store($new_request);
        if (!empty($result)){
            $respond["message"] = "Request successful";
            $respond["status"] = true;
        }
        else{
            $respond["message"] = "Request failed";
            $respond["status"] = false;
        }
    // }
    // else{
    //     $respond["message"] = "Username taken, please try another";
    //     $respond["status"] = false; 
    // }
    
    echo json_encode($respond);

    R::close();

?>