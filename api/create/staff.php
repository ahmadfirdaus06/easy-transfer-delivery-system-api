<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    include_once "../../config/redbean.php";
    include_once '../../model/user.php';
    include_once '../../model/staff_join_request.php';
    $user = new User();
    $join_request = new StaffJoinRequest();

    $data = json_decode(file_get_contents("php://input"));

    $join_request->id = htmlspecialchars(strip_tags($data->id));
    $join_request->nric = htmlspecialchars(strip_tags($data->nric));
    $join_request->username = htmlspecialchars(strip_tags($data->username));
    $join_request->password = htmlspecialchars(strip_tags($data->password));
    $join_request->name = htmlspecialchars(strip_tags($data->name));
    $join_request->email = htmlspecialchars(strip_tags($data->email));
    $join_request->phone_num = htmlspecialchars(strip_tags($data->phone_num));
    $application_status = htmlspecialchars(strip_tags($data->application_status));

    $existed = R::find($user->getTableName(), 'BINARY username = BINARY ?', [$join_request->username]);
    
    $respond = array();
    $respond["message"] = "";
    $respond["status"] = false;

    date_default_timezone_set("Asia/Kuala_Lumpur");

    function removeFromStaffJoinReq(StaffJoinRequest $join_request){    
        $to_be_removed = R::findOne($join_request->getTableName(), 'id = ?', [$join_request->id]);

        if(!empty($to_be_removed)){
            R::trash($to_be_removed);
            return true;
        }
        else{
            return false;
        }
    }

    if ($application_status == "approved"){
        if (empty($existed)){
            $new_user = R::dispense($user->getTableName());
            $new_user->nric = $join_request->nric;
            $new_user->username = $join_request->username;
            $new_user->password = $join_request->password;
            $new_user->name = $join_request->name;
            $new_user->email = $join_request->email;
            $new_user->phone_num = $join_request->phone_num;
            $new_user->user_status_id = "2";
            $new_user->updated_at = date("Y-m-d H:i:s");
            $new_user->user_type_id = "3";
            $result = R::store($new_user);
            
            if($result != null){
                if (removeFromStaffJoinReq($join_request)){
                    $respond["message"] = "Application Approved";
                    $respond["status"] = true;
                }
                else{
                    $respond["status"] = true;
                }
                
            }
            else{
                $respond["message"] = "Application Failed";
                $respond["status"] = false;
            }
        }
        else{
            $respond["message"] = "Staff Existed";
            $respond["status"] = false;
        }
    }
    else{
        if (removeFromStaffJoinReq($join_request)){
            $respond["message"] = "Application Rejected";
            $respond["status"] = true;
        }
        else{
            $respond["status"] = true;
        }
        
    }
    
    echo json_encode($respond);

    R::close();

?>