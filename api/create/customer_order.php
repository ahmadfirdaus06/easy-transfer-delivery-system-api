<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    include_once "../../config/redbean.php";
    include_once '../../model/payment.php';
    include_once '../../model/customer_order.php';
    include_once '../../model/card.php';

    $card = new Card();
    $payment = new Payment();
    $order = new Order();

    $data = json_decode(file_get_contents("php://input"));

    $order->pickup_date_time = htmlspecialchars(strip_tags($data->pickup_date_time));
    $order->dropoff_date_time = htmlspecialchars(strip_tags($data->dropoff_date_time));
    $order->dropoff_location = htmlspecialchars(strip_tags($data->dropoff_location));
    $order->pickup_location = htmlspecialchars(strip_tags($data->pickup_location));
    $order->customer_id = htmlspecialchars(strip_tags($data->customer_id));
    $order->parcel_size_id = htmlspecialchars(strip_tags($data->parcel_size_id));
    $order->delivery_distance = htmlspecialchars(strip_tags($data->delivery_distance));

    $payment->total_cost_delivery = htmlspecialchars(strip_tags($data->total_cost_delivery));
    $payment->payment_method_id = htmlspecialchars(strip_tags($data->payment_method_id));
    $payment->payment_status_id = htmlspecialchars(strip_tags($data->payment_status_id));

    $card->card_number = htmlspecialchars(strip_tags($data->card_number));
    $card->expiry_date = htmlspecialchars(strip_tags($data->expiry_date));
    $card->cvv = htmlspecialchars(strip_tags($data->cvv));

    $respond = array();
    $respond["message"] = "";
    $respond["status"] = false;

    date_default_timezone_set("Asia/Kuala_Lumpur");

    $new_order = R::dispense($order->getTableName());
    $new_order->pickup_date_time = $order->pickup_date_time;
    $new_order->dropoff_date_time = $order->dropoff_date_time;
    $new_order->pickup_location = $order->pickup_location;
    $new_order->dropoff_location = $order->dropoff_location;
    $new_order->customer_id = $order->customer_id;
    $new_order->parcel_size_id = $order->parcel_size_id;
    $new_order->delivery_distance = $order->delivery_distance;
    $new_order->updated_at = date("Y-m-d H:i:s");
    $result = R::store($new_order);

    if (!empty($result)){
        $new_payment = R::dispense($payment->getTableName());
        $new_payment->customer_order_id = $result;
        $new_payment->total_cost_delivery = $payment->total_cost_delivery;
        $new_payment->payment_method_id = $payment->payment_method_id;
        $new_payment->payment_status_id = $payment->payment_status_id;
        $new_payment->updated_at = date("Y-m-d H:i:s");
        $result = R::store($new_payment);

        if (!empty($result && $payment->payment_method_id == "2")){
            $new_card = R::dispense($card->getTableName());
            $new_card->payment_id = $result;
            $new_card->cvv = $card->cvv;
            $new_card->card_number = $card->card_number;
            $new_card->expiry_date = $card->expiry_date;
            $new_card->updated_at = date("Y-m-d H:i:s");
            $result = R::store($new_card);
            
            if (!empty($result)){
                $respond["message"] = "Order Sent";
                $respond["status"] = true;
            }
            else{
                $respond["message"] = "Order Failed";
                $respond["status"] = false;
            }
        }else if (!empty($result)){
            $respond["message"] = "Order Sent";
            $respond["status"] = true;
        }
        else{
            $respond["message"] = "Order Failed";
            $respond["status"] = false;
        }
    }

    echo json_encode($respond);

    R::close();

?>