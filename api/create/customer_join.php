<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    include_once "../../config/redbean.php";
    include_once '../../model/user.php';
    $user = new User();

    $data = json_decode(file_get_contents("php://input"));

    $user->nric = htmlspecialchars(strip_tags($data->nric));
    $user->username = htmlspecialchars(strip_tags($data->username));
    $user->password = htmlspecialchars(strip_tags($data->password));
    $user->name = htmlspecialchars(strip_tags($data->name));
    $user->email = htmlspecialchars(strip_tags($data->email));
    $user->phone_num = htmlspecialchars(strip_tags($data->phone_num));
    
    $respond = array();
    $respond["message"] = "";
    $respond["status"] = false;

    date_default_timezone_set("Asia/Kuala_Lumpur");

    $new_request = R::dispense($user->getTableName());
    $new_request->nric = $user->nric;
    $new_request->username = $user->username;
    $new_request->password = password_hash($user->password, PASSWORD_BCRYPT);
    $new_request->name = $user->name;
    $new_request->email = $user->email;
    $new_request->phone_num = $user->phone_num;
    $new_request->updated_at = date("Y-m-d H:i:s");
    $new_request->user_type_id = 2;
    $new_request->user_status_id = 1;
    $result = R::store($new_request);
    if (!empty($result)){
        $respond["message"] = "Registration successful";
        $respond["status"] = true;
    }
    else{
        $respond["message"] = "Registration failed";
        $respond["status"] = false;
    }
    
    echo json_encode($respond);

    R::close();

?>