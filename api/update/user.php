<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: PUT');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin,Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    include_once "../../config/redbean.php";
    include_once '../../model/user.php';
    $user = new User();

    $data = json_decode(file_get_contents("php://input"));

    $user->id = htmlspecialchars(strip_tags($data->id));
    $user->name = htmlspecialchars(strip_tags($data->name));
    $user->username = htmlspecialchars(strip_tags($data->username));
    $user->email = htmlspecialchars(strip_tags($data->email));
    $user->phone_num = htmlspecialchars(strip_tags($data->phone_num));
    $user->nric = htmlspecialchars(strip_tags($data->nric));
    $user->user_status_id = htmlspecialchars(strip_tags($data->user_status_id));
    if (property_exists($data, "password")){
        $user->password = htmlspecialchars(strip_tags($data->password));
    }

    date_default_timezone_set("Asia/Kuala_Lumpur");

    $respond = array();
    $respond["message"] = "";
    $respond["status"] = false;

    $fetched_user = R::findOne($user->getTableName(), '
    id = ?'
    , [$user->id]);
    
    $fetched_user->name = $user->name;
    $fetched_user->username = $user->username;
    $fetched_user->email = $user->email;
    $fetched_user->phone_num = $user->phone_num;
    $fetched_user->nric = $user->nric;
    $fetched_user->user_status_id = $user->user_status_id;
    if ($user->password != null || $user->password != ""){
        $fetched_user->password = password_hash($user->password, PASSWORD_BCRYPT);
    }
    $fetched_user->updated_at = date("Y-m-d H:i:s");

    $result = R::store($fetched_user);

    if($result != null){
        $respond["message"] = "Changes Saved";
        $respond["status"] = true;
    }
    else{
        $respond["message"] = "Cannot Save Changes";
        $respond["status"] = false;
    }
    echo json_encode($respond);

    R::close();

?>