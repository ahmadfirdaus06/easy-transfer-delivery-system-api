<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    include_once "../../config/redbean.php";
    include_once '../../model/customer_order.php';
    include_once '../../model/user.php';
    include_once '../../model/payment.php';
    include_once '../../model/report.php';

    $report = new Report();
    $user = new User();
    $order = new Order();
    $payment = new Payment();

    $data = json_decode(file_get_contents("php://input"));
    
    $flag = htmlspecialchars(strip_tags($data->update_flag));
    $order->id = htmlspecialchars(strip_tags($data->order_id));
    $update_order = R::findOne($order->getTableName(), ' id = ?', [$order->id]);
    $result = null;

    $respond = array();
    $respond["message"] = "";
    $respond["status"] = false;

    date_default_timezone_set("Asia/Kuala_Lumpur");

    if (!empty($update_order)){
        switch($flag){
            case "ACCEPT":
                $order->driver_id = htmlspecialchars(strip_tags($data->driver_id));
                $in_hand_order = R::find($order->getTableName(), ' 
                driver_id = ? AND
                order_status_id < 3', 
                [$order->driver_id]);
                if (empty($in_hand_order)){
                    $update_order->driver_id = $order->driver_id;
                    $update_order->order_accept_by = date("Y-m-d H:i:s");
                    $update_order->updated_at = date("Y-m-d H:i:s");
                    $result = R::store($update_order);
                    if (!empty($result)){
                        $respond["message"] = "Order approved";
                        $respond["status"] = true;
                    }
                    else{
                        $respond["message"] = "Request Failed, Please try again later";
                        $respond["status"] = false;
                    }
                }
                else{
                    $respond["message"] = "Cannot accept more than 1 request";
                    $respond["status"] = false;
                }
            break;
    
            case "PICKUP":
                $update_payment = R::findOne($payment->getTableName(), ' customer_order_id = ?', [$order->id]);
                
                if (!empty($update_payment) && !empty($update_order)){
                    if ($update_payment->payment_status_id == 1){
                        $update_payment->updated_at = date("Y-m-d H:i:s");
                        $update_payment->payment_status_id = 2;
                        $result = R::store($update_payment);
                        
                        if (!empty($result)){
                            $respond["message"] = "Payment verified";
                            $respond["status"] = true;
                        }
                        else{
                            $respond["message"] = "Request Failed, Please try again later";
                            $respond["status"] = false;
                        }
                        
                    }
                    else{
                        $respond["message"] = "Payment verified";
                        $respond["status"] = true;
                    }
                    $update_order->updated_at = date("Y-m-d H:i:s");
                    $update_order->order_status_id = 2;
                    $result = R::store($update_order);
                }
                else{
                    $respond["message"] = "Request Failed, Please try again later";
                    $respond["status"] = false;
                }
            break;
    
            case "DELIVERED":
                $order->driver_id = htmlspecialchars(strip_tags($data->driver_id));
                $report->total_earned = htmlspecialchars(strip_tags($data->total_earned));
                $new_report = R::dispense($report->getTableName());
                $new_report->customer_order_id = $order->id;
                $new_report->total_earned = $report->total_earned;
                $new_report->driver_id = $order->driver_id;
                $new_report->updated_at = date("Y-m-d H:i:s");
                $update_order->updated_at = date("Y-m-d H:i:s");
                $update_order->order_status_id = 3;
                $result1 = R::store($update_order);
                $result2 = R::store($new_report);
                
                if (!empty($result1) && !empty($result2)){
                    $respond["message"] = "Order delivered";
                    $respond["status"] = true;
                }
                else{
                    $respond["message"] = "Request Failed, Please try again later";
                    $respond["status"] = false;
                }
            break;
    
            case "CLOSE":
                $report->total_rating = htmlspecialchars(strip_tags($data->total_rating));
                $update_report = R::findOne($report->getTableName(), ' customer_order_id = ?', [$update_order->id]);
                $update_report->total_rating = $report->total_rating;
                $update_report->updated_at = date("Y-m-d H:i:s");
                $update_order->order_status_id = 4;
                $update_order->updated_at = date("Y-m-d H:i:s");
                $result1 = R::store($update_report);
                $result2 = R::store($update_order);
                if (!empty($result1) && !empty($result2)){
                    $respond["message"] = "Thank you for rating our driver";
                    $respond["status"] = true;
                }
                else{
                    $respond["message"] = "Request Failed, Please try again later";
                    $respond["status"] = false;
                }
            break;
        }
    }
    else{
        $respond["message"] = "Order not found";
        $respond["status"] = false;
    }

    echo json_encode($respond);

    R::close();
?>