<?php    
    require_once $_SERVER['DOCUMENT_ROOT']."/easy-transfer-delivery-system-api/lib/rb.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/easy-transfer-delivery-system-api/config/db.php";
    $db = new Database();
    $host = $db->host;
    $port = $db->port;
    $db_name = $db->db_name;
    $username = $db->username;
    $password = $db->password;

    R::setup(new PDO('mysql:host=' . gethostbyname($host) . ';port=' . $port . ';dbname=' . $db_name, $username, $password));
?>