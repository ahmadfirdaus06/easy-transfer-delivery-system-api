<?php

   class Database{
         public $host = 'localhost';
         public $port = "3306";
         public $db_name = 'easy_transfer_delivery_system';
         public $username = 'root';
         public $password = '';
         public $conn;

         public function connect(){
            $this->conn = null;

            try{
               $this->conn = new PDO('mysql:host=' . gethostbyname($this->host) . ';port=' . $this->port . ';dbname=' . $this->db_name, $this->username, $this->password);
               // $this->conn = new PDO('mysql:host=' . gethostbyname($this->host) . ';dbname=' . $this->db_name, $this->username, $this->password);
               $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
               
            }catch(PDOException $e){
               // echo '<b><br>Error: <span style="color: #ff4444">' . $e->getMessage(). '</span></b>';
               echo "<script>console.log(`". $e->getMessage() ."`)</script>";
               return null;
               
            }
            return $this->conn;
         }
   }
?>