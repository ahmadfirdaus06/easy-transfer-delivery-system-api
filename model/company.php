<?php
    class Company{
        private $table = 'dt_company_information';

        //properties
        public $id;
        public $company_name;
        public $establish_at;
        public $about;
        public $desc;
        public $updated_at;

        public function __construct(){
        }

        public function getTableName(){
            return $this->table;
        }
    }
?>