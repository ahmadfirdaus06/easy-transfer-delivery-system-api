<?php
    class Payment{
        private $table = 'dt_payment';

        //properties
        public $id;
        public $total_cost_delivery;
        public $payment_status_id;
        public $payment_method_id;
        public $created_at;
        public $updated_at;
        public $customer_order_id;

        public function __construct(){
        }

        public function getTableName(){
            return $this->table;
        }
    }
?>