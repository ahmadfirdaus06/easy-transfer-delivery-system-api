<?php
    class ParcelSize{
        private $table = 'lt_parcel_size_rate';

        //properties
        public $id;
        public $desc;
        public $rate;

        public function __construct(){
        }

        public function getTableName(){
            return $this->table;
        }
    }
?>