<?php
    class Card{
        private $table = 'dt_card_information';

        //properties
        public $id;
        public $card_number;
        public $expiry_date;
        public $cvv;
        public $created_at;
        public $updated_at;
        public $payment_id;

        public function __construct(){
        }

        public function getTableName(){
            return $this->table;
        }
    }
?>