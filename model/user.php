<?php
    class User{
        private $table = 'dt_user';

        //properties
        public $id;
        public $nric;
        public $username;
        public $password;
        public $name;
        public $email;
        public $created_at;
        public $updated_at;
        public $last_login;
        public $last_logout;
        public $phone_num;
        public $user_type_id;
        public $user_status_id;

        public function __construct(){
        }

        public function getTableName(){
            return $this->table;
        }
    }
?>