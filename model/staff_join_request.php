<?php
    class StaffJoinRequest{
        private $table = 'dt_staff_join_request';

        //properties
        public $id;
        public $nric;
        public $username;
        public $password;
        public $name;
        public $email;
        public $created_at;
        public $phone_num;

        public function __construct(){
        }

        public function getTableName(){
            return $this->table;
        }
    }
?>