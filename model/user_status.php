<?php
    class UserStatus{
        private $table = 'lt_user_status';

        //properties
        public $id;
        public $desc;

        public function __construct(){
        }

        public function getTableName(){
            return $this->table;
        }
    }
?>