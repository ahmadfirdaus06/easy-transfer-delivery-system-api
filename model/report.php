<?php
    class Report{
        private $table = 'dt_report';

        //properties
        public $id;
        public $order_id;
        public $driver_id;
        public $total_rating;
        public $total_earned;
        public $created_at;
        public $updated_at;

        public function __construct(){
        }

        public function getTableName(){
            return $this->table;
        }
    }
?>