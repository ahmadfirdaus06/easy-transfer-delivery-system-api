<?php
    class Order{
        private $table = 'dt_customer_order';

        //properties
        public $id;
        public $pickup_date_time;
        public $dropoff_date_time;
        public $pickup_location;
        public $dropoff_location;
        public $delivery_distance;
        public $order_accept_by;
        public $customer_id;
        public $created_at;
        public $updated_at;
        public $driver_id;
        public $order_status_id;
        public $parcel_size_id;

        public function __construct(){
        }

        public function getTableName(){
            return $this->table;
        }
    }
?>